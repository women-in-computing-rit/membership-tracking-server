from flask import Flask
from flask_cors import CORS
from subprocess import Popen, PIPE, STDOUT

import os


def create_app():
    app = Flask('signin_server', instance_relative_config=True)

    CORS(app)

    from wic_signin.endpoints import management
    app.register_blueprint(management.bp)

    return app


wic_signin = create_app()