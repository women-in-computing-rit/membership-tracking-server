import requests

from concurrent.futures import ThreadPoolExecutor

from wic_signin.util.hash import hash_id
from wic_signin.util.database import get_db
from wic_signin.objects.member import Member


def main():
    db = get_db()
    while True:
        cardSwipe = hash_id(input('Swipe Card'))

        member = db.query(Member).filter_by(uid=cardSwipe).first()
        print(member)


if __name__ == '__main__':
    main()