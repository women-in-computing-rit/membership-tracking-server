from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from wic_signin.util.database import BaseModel


class Member(BaseModel):
    __tablename__ = 'member'

    id = Column(Integer, primary_key=True)
    uid = Column(String(64), nullable=False)
    first_name = Column(String(32), nullable=False)
    last_name = Column(String(32), nullable=False)
    major = Column(String(64), nullable=False)
    year = Column(Integer, nullable=False)

    attendance = relationship('Attendance')

    def __repr__(self):
        return f'Member(uid:{self.uid} name:{self.first_name} {self.last_name} major:{self.major} year{self.year})'


def generateMemberFromJson(memberJson):
    tmp = Member()

    tmp.uid = memberJson.get('uid')
    tmp.first_name = memberJson.get('first_name')
    tmp.last_name = memberJson.get('last_name')
    tmp.major = memberJson.get('major')
    tmp.year = memberJson.get('year')

    return tmp