from sqlalchemy import Column, Integer, String, ForeignKey

from wic_signin.util.database import BaseModel


class Attendance(BaseModel):
    __tablename__ = 'attendance'

    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer, nullable=False)
    purpose = Column(String(32), nullable=False)

    member_id = Column(Integer, ForeignKey('member.id'))