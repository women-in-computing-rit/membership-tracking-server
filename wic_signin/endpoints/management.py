import json

from os import environ
from datetime import datetime
from flask import Blueprint, request

from wic_signin.objects.member import Member, generateMemberFromJson
from wic_signin.objects.attendance import Attendance
from wic_signin.util.database import get_db

bp = Blueprint('data_management', __name__, url_prefix='/dm')
db = get_db()


@bp.route('/swipe', methods=['POST'])
def swipe_member():
    newSwipe = Attendance()
    newSwipe.member_id = request.json.get('uid')
    newSwipe.purpose = environ.get('SWIPE_TYPE')
    newSwipe.timestamp = int(datetime.utcnow().timestamp()*1000)

    db.add(newSwipe)
    db.commit()

    return json.dumps({'msg': 'success'}), 200


@bp.route('/swipe_type', methods=['POST'])
def change_swipe_type():
    swipe_type = request.json.get('type')
    environ['SWIPE_TYPE'] = swipe_type


@bp.route('/create_member', methods=['POST'])
def create_member():
    newMember = generateMemberFromJson(request.json)

    db.add(newMember)
    db.commit()

    return json.dumps({'msg': 'success'}), 200