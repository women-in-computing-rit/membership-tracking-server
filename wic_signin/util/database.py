from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import  declarative_base

import os


BaseModel = declarative_base()


def get_db():
    engine = create_engine(os.environ.get('DATABASE_URI', 'sqlite:///:memory:'))
    db = sessionmaker(bind=engine)()

    return db