import os

from hashlib import sha3_256

def hash_id(id):
    hasher = sha3_256()

    hasher.update(os.environ.get('ID_SALT').encode('utf8'))
    hasher.update(id.encode('utf8'))

    return hasher.hexdigest()