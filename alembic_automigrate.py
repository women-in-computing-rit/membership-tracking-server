from os import environ
from uuid import uuid4
from json import load

import argparse
import subprocess

parser = argparse.ArgumentParser(description='Read Migration Name')
parser.add_argument('-m', type=str, help='Description for your Migration')
parser.add_argument('-am', '--auto-migrate', action="store_true")
parser.add_argument('-dg', '--downgrade', action="store_true")
args = parser.parse_args()


def setEnvironmentVariables():
    launchJson = ''
    with open('.vscode/launch.json') as jsonFile:
        launchJson = load(jsonFile)

    environVariables = launchJson.get('configurations')[0].get('env')

    for pair in environVariables.items():
        environ.putenv(*pair)


def main():
    print(args)
    msg = f'auto generated msg id({uuid4().hex})'

    setEnvironmentVariables()

    if args.m is not None:
        msg = args.m

        subprocess.run(["alembic", "revision", "--autogenerate", "-m", f'"${msg}"'])

    if args.auto_migrate:
        subprocess.run(['alembic', 'upgrade', 'head'])

    if args.downgrade:
        subprocess.run(['alembic', 'downgrade', '-1'])


if __name__ == '__main__':
    main()